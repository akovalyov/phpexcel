<?php
//require_once '../Parser/CssParser.php';
require_once 'StylePhpExcel.php';

class CssPhpExcelTranslator
{

    protected $dictionnary = array(
        'vertical-align' => 'vAlign',
        'phpexcel-number-format-code' => 'numberFormatCode',
        'phpexcel-number-format' => 'numberFormat',
        'text-indent' => 'textIndent',
        'text-align' => 'alignment',
        'border' => 'border',
        'border-left' => 'border',
        'border-right' => 'border',
        'border-top' => 'border',
        'border-bottom' => 'border',
        'margin' => 'margin',
        'padding' => 'padding',
        'font-weight' => 'font',
        'font-family' => 'font',
        'font-size' => 'font',
        'color' => 'font',
        'background' => 'background',
        'min-width' => 'noop'

    );

    /**
     *
     * @param string $strCss
     * @return \Fuller\ReportBundle\Templating\Generator\PhpExcel\Reader\Html\Translator\StylePhpExcel
     */
    public function translate($strCss)
    {
        $strCss = "element.style { $strCss }";

        $css = new CssParser();
        $css->ParseStr($strCss);

        $arr = $css->css;
        $styleAry = array();
        $stylePhpExcel = new StylePhpExcel();
        foreach ($arr['element.style'] as $key => $rule) {
            if (isset($this->dictionnary[$key])) {
                $result = $this->{$this->dictionnary[$key]}($key, $rule);
                $styleAry = self::array_merge_recursive_distinct($styleAry, $result);

            } else {
                throw new \Exception('Rule ' . $key . ' is not implemented');
            }
        };

        $stylePhpExcel->setStyle($styleAry);

        return $stylePhpExcel;
    }

    function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset ($merged [$key]) && is_array($merged [$key])) {
                $merged [$key] = self::array_merge_recursive_distinct($merged [$key], $value);
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    protected function vAlign($key, $rule)
    {
        $mValue = array(
            'top' => 'VERTICAL_TOP',
            'middle' => 'VERTICAL_CENTER',
            'bottom' => 'VERTICAL_BOTTOM',
            'phpexcel-justify' => 'VERTICAL_JUSTIFY',
        );

        $phpExcelStyleArray = array();
        switch (true) {
            case key_exists($rule, $mValue):
                $phpExcelStyleArray['alignment']['vertical'] = constant('\PHPExcel_Style_Alignment::' . $mValue[$rule]);
                break;

            default:
                $phpExcelStyleArray['alignment']['vertical'] = constant('\PHPExcel_Style_Alignment::' . $mValue['top']);
                break;
        }

        return $phpExcelStyleArray;
    }

    protected function numberFormatCode($key, $rule)
    {
        $phpExcelStyleArray = array();
        $phpExcelStyleArray['numberformat']['code'] = $rule;

        return $phpExcelStyleArray;
    }

    protected function numberFormat($rule)
    {
        $mValue = array(
            'format_general' => 'FORMAT_GENERAL',
            'format_text' => 'FORMAT_TEXT',
            'format_number' => 'FORMAT_NUMBER',
            'format_number_00' => 'FORMAT_NUMBER_00',
            'format_number_comma_separated1' => 'FORMAT_NUMBER_COMMA_SEPARATED1',
            'format_number_comma_separated2' => 'FORMAT_NUMBER_COMMA_SEPARATED2',
            'format_percentage' => 'FORMAT_PERCENTAGE',
            'format_percentage_00' => 'FORMAT_PERCENTAGE_00',
            'format_date_yyyymmdd2' => 'FORMAT_DATE_YYYYMMDD2',
            'format_date_yyyymmdd' => 'FORMAT_DATE_YYYYMMDD',
            'format_date_ddmmyyyy' => 'FORMAT_DATE_DDMMYYYY',
            'format_date_dmyslash' => 'FORMAT_DATE_DMYSLASH',
            'format_date_dmyminus' => 'FORMAT_DATE_DMYMINUS',
            'format_date_dmminus' => 'FORMAT_DATE_DMMINUS',
            'format_date_myminus' => 'FORMAT_DATE_MYMINUS',
            'format_date_xlsx14' => 'FORMAT_DATE_XLSX14',
            'format_date_xlsx15' => 'FORMAT_DATE_XLSX15',
            'format_date_xlsx16' => 'FORMAT_DATE_XLSX16',
            'format_date_xlsx17' => 'FORMAT_DATE_XLSX17',
            'format_date_xlsx22' => 'FORMAT_DATE_XLSX22',
            'format_date_datetime' => 'FORMAT_DATE_DATETIME',
            'format_date_time1' => 'FORMAT_DATE_TIME1',
            'format_date_time2' => 'FORMAT_DATE_TIME2',
            'format_date_time3' => 'FORMAT_DATE_TIME3',
            'format_date_time4' => 'FORMAT_DATE_TIME4',
            'format_date_time5' => 'FORMAT_DATE_TIME5',
            'format_date_time6' => 'FORMAT_DATE_TIME6',
            'format_date_time7' => 'FORMAT_DATE_TIME7',
            'format_date_time8' => 'FORMAT_DATE_TIME8',
            'format_date_yyyymmddslash' => 'FORMAT_DATE_YYYYMMDDSLASH',
            'format_currency_usd_simple' => 'FORMAT_CURRENCY_USD_SIMPLE',
            'format_currency_usd' => 'FORMAT_CURRENCY_USD',
            'format_currency_eur_simple' => 'FORMAT_CURRENCY_EUR_SIMPLE',
        );

        $phpExcelStyleArray = array();
        switch (true) {
            case key_exists($rule, $mValue):
                $phpExcelStyleArray['numberformat']['format'] = constant('\PHPExcel_Style_NumberFormat::' . $mValue[strtolower($rule)]);
                break;
            default:
                $phpExcelStyleArray['numberformat']['format'] = constant('\PHPExcel_Style_NumberFormat::' . $mValue['format_general']);
                break;
        }

        return $phpExcelStyleArray;
    }

    protected function textIndent($key, $rule)
    {
        $phpExcelStyleArray = array();
        $phpExcelStyleArray['alignment']['indent'] = (int)$rule / 9;

        return $phpExcelStyleArray;
    }

    protected function alignment($key, $rule)
    {
        $array = array('alignment' => array(
//            'shrinkToFit' => true,
            'wrap' => true
        ));

        /**
         *    const HORIZONTAL_GENERAL                = 'general';
        const HORIZONTAL_LEFT					= 'left';
        const HORIZONTAL_RIGHT					= 'right';
        const HORIZONTAL_CENTER					= 'center';
        const HORIZONTAL_CENTER_CONTINUOUS		= 'centerContinuous';
        const HORIZONTAL_JUSTIFY				= 'justify';

        Vertical alignment styles
        const VERTICAL_BOTTOM					= 'bottom';
        const VERTICAL_TOP						= 'top';
        const VERTICAL_CENTER					= 'center';
        const VERTICAL_JUSTIFY					= 'justify';
         */
        switch ($rule) {
            case 'left':
                $array['alignment']['horizontal'] = constant('\PHPExcel_Style_Alignment::LEFT');
                break;
            case 'center':
                $array['alignment']['horizontal'] = constant('\PHPExcel_Style_Alignment::HORIZONTAL_CENTER');
                break;
            default:
                throw new \Exception('Alignment rule ' . $key . ' ' . $rule . ' is not implemented in CssPhPExcelTranslator::alignment()');
        }
        return $array;
    }

    protected function border($key, $rule)
    {
        $array = array('borders' => array()

        );
        if ($key == 'border') {
            if ($rule != 'none') {
                list($width, $type, $rgb) = explode(' ', $rule);
                foreach (array('top', 'left', 'bottom', 'right') as $k) {
                    $array['borders'][$k]['style'] = PHPExcel_Style_Border::BORDER_THICK;
                    $array['borders'][$k]['color']['rgb'] = strtoupper(str_replace('#', '', $rgb));
                }
            }
        } elseif ($key == 'border-left' || $key == 'border-right' || $key == 'border-top' || $key == 'border-bottom') {
            $k = explode('-', $key);
            $color = explode(' ', $rule);
            $array['borders'][$k[1]]['style'] = $rule == 'none' ? PHPExcel_Style_Border::BORDER_NONE : PHPExcel_Style_Border::BORDER_THICK;
            $array['borders'][$k[1]]['color']['rgb'] = $rule == 'none' ? '000000' : strtoupper(str_replace('#', '', $color[2]));
//            $array['borders'][$k[2]]['color']['rgb'] = str_replace('#', '', $rgb);
        } else {
            throw new \Exception('Border rule ' . $key . ':' . $rule . ' is not implemented in CssPhPExcelTranslator::border()');

        }
        return $array;

    }

    /**
     * there is no margin in Excel
     */
    protected function margin()
    {
        return array();
    }

    protected function padding($key, $rule)
    {
        return array('alignment' => array(
            'indent' => intval($rule)
        ));
    }

    protected function font($key, $rule)
    {
//        $array = array(
//            'name' => 'Arial',
//            'bold' => TRUE,
//            'italic' => FALSE,
//            'size' => '16',
//            'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
//            'strike' => FALSE,
//            'color' => array(
//                'argb' => 'FF000000'
//            )
//        );
        $array = array();
        if ($key == 'font-weight') {
            $array['bold'] = TRUE;
        } elseif ($key == 'color') {
            $array['color']['rgb'] = strtoupper(str_replace('#', '', $rule));
        } elseif ($key == 'font-family') {
            $array['name'] = $rule;
        } elseif ($key == 'font-size') {

            $array['size'] = intval($rule);
        } else {
            throw new \Exception('Font rule ' . $key . ' is not implemented');
        }

        return array('font' => $array);
    }

    protected function background($key, $value)
    {
        if (!strstr($value, '#')) {
            throw new \Exception('non-hex background is not inplemented');
        }
        return array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => strtoupper(str_replace('#', '', $value)))
            )
        );

    }

    function noop()
    {
        return array();
    }


}